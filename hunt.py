import requests
from bs4 import BeautifulSoup
from urllib.request import urlopen
import xlrd
import csv

import re
from collections import deque
from urllib.parse import urlsplit
from xlutils.copy import copy

def import_excel(name):
	book = xlrd.open_workbook(name)
	sheet = book.sheet_by_index(0)
	wb = copy(book)
	sheet_new = wb.get_sheet(0)
	
	
	row_no,col_no=0,1
	for i in range(0,2039): #Update number of rows here
		try:
			contact_no,mail_id,address = get_all_pages(str(sheet.row_values(i)[0]),i)
		except:
			print("-----ERROR-----")
			contact_nomail_id="404","404"
		contact_no = contact_no[:3275] #Longer than 3276 will poduce error.
		mail_id = mail_id[:3275]
		#address = address[:3275]
		if contact_no=="set()":
			contact_no="404"
		elif contact_no.startswith("{"):
			contact_no=contact_no[1:-1]
		if mail_id=="set()":
			mail_id="404"
		elif mail_id.startswith("{"):
			mail_id=mail_id[1:-1]
		sheet_new.write(row_no,1,contact_no)
		sheet_new.write(row_no,2,mail_id)
		#sheet_new.write(row_no,3,address)
		row_no+=1
		wb.save("Output.xls")


def get_all_pages(url,i):
	print("\n\n-------",i,url,"-------\n")
	urls = deque([url])
	processed_urls = set()

	contact_no = set()
	mail_id = set()
	address = "404"

	while len(urls):
		url = urls.popleft()
		processed_urls.add(url)

		parts = urlsplit(url)
		base_url = "{0.scheme}:/{0.netloc}".format(parts)
		path = url[:url.rfind('/')+1] if '/' in parts.path else url

		print(".",len(urls),end=" ")

		try:
			response = requests.get(url)
			page = urlopen(url)
			page_str = (page.read()).decode('utf-8')
		except:
			continue

		contact = re.findall(r"\d\d\d[-.]\d\d\d[-.]\d\d\d\d",page_str)
		mail = re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+",page_str)

		address_name = re.findall(r"address",page_str,re.IGNORECASE)

		
		#check if contact and mail both found; if yes -> get the div that contains it.
		soup = BeautifulSoup(response.text,'html.parser')
		
		if (len(address_name)>0):
			address_divs = soup.find_all(text=re.compile(address_name[0]))
			try:
				address= address_divs[0]
			except:
				continue

		for i in contact:
			contact_no.add(i)
		for i in mail:
			mail_id.add(i)


		if (len(contact_no)>0 and len(mail_id)>0):
			print("\n",contact_no,"---",mail_id)
			return(str(contact_no),str(mail_id),address)

		

		for anchor in soup.find_all("a"):
			link = anchor.attrs["href"] if "href" in anchor.attrs else ''
			if link.startswith('/'):
				link = base_url + link
			elif not link.startswith('http'):
				link = path + link
			if not link in urls and not link in processed_urls:
				urls.append(link)

		if (len(urls)<1):
			print("\n",contact_no,"---",mail_id)
			if (len(contact_no)>0 and len(mail_id)>0):
				return("404","404")
			return(str(contact_no),str(mail_id),address)
	return("404","404","404")


import_excel("SkyOutdoors.xls")